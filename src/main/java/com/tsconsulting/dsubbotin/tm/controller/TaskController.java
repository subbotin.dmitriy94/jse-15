package com.tsconsulting.dsubbotin.tm.controller;

import com.tsconsulting.dsubbotin.tm.api.controller.ITaskController;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownStatusException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() throws EmptyNameException,
            EmptyDescriptionException {
        showMessage("Enter name:");
        String name = TerminalUtil.nextLine();
        showMessage("Enter description:");
        String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        showMessage("[Create]");
    }

    @Override
    public void showTasks() throws TaskNotFoundException {
        showMessage("Enter sort:");
        showMessage(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        boolean containsFlag = Arrays.asList(Sort.values()).contains(sort);
        if (sort == null || sort.isEmpty() || !containsFlag) tasks = taskService.findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            showMessage(sortType.getDisplayName());
            tasks = taskService.findAll(sortType.getComparator());
        }
        if (tasks == null || tasks.size() == 0) throw new TaskNotFoundException();
        int index = 1;
        for (Task task : tasks) showMessage(index++ + ". " + task);
    }

    @Override
    public void clearTasks() {
        taskService.clear();
        showMessage("[Clear]");
    }

    @Override
    public void showById() throws EmptyIdException,
            TaskNotFoundException {
        showMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        showTask(task);
    }

    @Override
    public void showByIndex() throws IndexIncorrectException,
            TaskNotFoundException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            final Task task = taskService.findByIndex(index);
            showTask(task);
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }
    }

    @Override
    public void showByName() throws EmptyNameException,
            TaskNotFoundException {
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findByName(name);
        showTask(task);
    }

    @Override
    public void removeById() throws EmptyIdException,
            TaskNotFoundException {
        showMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        showMessage("[Task removed]");
    }

    @Override
    public void removeByIndex() throws EmptyIndexException,
            IndexIncorrectException,
            TaskNotFoundException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            final Task task = taskService.removeByIndex(index);
            showMessage("[Task removed]");
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }
    }

    @Override
    public void removeByName() throws EmptyNameException,
            TaskNotFoundException {
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeByName(name);
        showMessage("[Task removed]");
    }

    @Override
    public void updateById() throws EmptyIdException,
            TaskNotFoundException,
            EmptyNameException {
        showMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        Task foundTask = taskService.findById(id);
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        showMessage("Enter description:");
        final String description = TerminalUtil.nextLine();
        Task updatedTask = taskService.updateById(id, name, description);
        showMessage("[Task updated]");
    }

    @Override
    public void updateByIndex() throws IndexIncorrectException,
            EmptyNameException,
            TaskNotFoundException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            Task foundTask = taskService.findByIndex(index);
            showMessage("Enter name:");
            final String name = TerminalUtil.nextLine();
            showMessage("Enter description:");
            final String description = TerminalUtil.nextLine();
            Task updatedTask = taskService.updateByIndex(index, name, description);
            showMessage("[Task updated]");
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }
    }

    @Override
    public void startById() throws EmptyIdException,
            TaskNotFoundException {
        showMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startById(id);
    }

    @Override
    public void startByIndex() throws IndexIncorrectException,
            TaskNotFoundException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            final Task task = taskService.startByIndex(index);
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }
    }

    @Override
    public void startByName() throws EmptyNameException,
            TaskNotFoundException {
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startByName(name);
    }

    @Override
    public void finishById() throws EmptyIdException,
            TaskNotFoundException {
        showMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishById(id);
    }

    @Override
    public void finishByIndex() throws IndexIncorrectException,
            TaskNotFoundException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            final Task task = taskService.finishByIndex(index);
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }
    }

    @Override
    public void finishByName() throws EmptyNameException,
            TaskNotFoundException {
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishByName(name);
    }

    @Override
    public void updateStatusById() throws EmptyIdException,
            TaskNotFoundException,
            UnknownStatusException {
        showMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        Task foundTask = taskService.findById(id);
        showMessage("Enter status:");
        showMessage(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        try {
            Status status = Status.valueOf(statusValue);
            final Task updatedStatusTask = taskService.updateStatusById(id, status);
            showMessage("[Updated task status]");
        } catch (IllegalArgumentException e) {
            throw new UnknownStatusException();
        }

    }

    @Override
    public void updateStatusByIndex() throws IndexIncorrectException,
            UnknownStatusException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            Task foundTask = taskService.findByIndex(index);
            showMessage("Enter status:");
            showMessage(Arrays.toString(Status.values()));
            final String statusValue = TerminalUtil.nextLine();
            try {
                Status status = Status.valueOf(statusValue);
                final Task updatedStatusTask = taskService.updateStatusByIndex(index, status);
                showMessage("[Updated task status]");
            } catch (IllegalArgumentException e) {
                throw new UnknownStatusException();
            }
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }
    }

    @Override
    public void updateStatusByName() throws EmptyNameException,
            TaskNotFoundException,
            UnknownStatusException {
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        Task foundTask = taskService.findByName(name);
        showMessage("Enter status:");
        showMessage(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        try {
            Status status = Status.valueOf(statusValue);
            final Task updatedStatusTask = taskService.updateStatusByName(name, status);
            showMessage("[Updated task status]");
        } catch (IllegalArgumentException e) {
            throw new UnknownStatusException();
        }
    }

    private void showMessage(String message) {
        System.out.println(message);
    }

    private void showTask(final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        showMessage("Id: " + task.getId() + "\n" +
                "Name: " + task.getName() + "\n" +
                "Description: " + task.getDescription() + "\n" +
                "Status: " + task.getStatus().getDisplayName() + "\n" +
                "Project id: " + task.getProjectId() + "\n" +
                "Create date: " + task.getCreateDate() + "\n" +
                "Start date: " + task.getStartDate()
        );
    }

}