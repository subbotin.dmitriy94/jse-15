package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(final Task task);

    void remove(final Task task);

    List<Task> findAll();

    List<Task> findAll(final Comparator<Task> comparator);

    void clear();

    boolean existById(final String id) throws TaskNotFoundException;

    Task findById(final String id) throws TaskNotFoundException;

    Task findByIndex(final int index) throws IndexIncorrectException;

    Task findByName(final String name) throws TaskNotFoundException;

    Task removeById(final String id) throws TaskNotFoundException;

    Task removeByIndex(final int index) throws IndexIncorrectException;

    Task removeByName(final String name) throws TaskNotFoundException;

    Task updateBuyId(final String id, final String name, final String description) throws TaskNotFoundException;

    Task updateBuyIndex(final int index, final String name, final String description) throws IndexIncorrectException;

    Task startById(final String id) throws TaskNotFoundException;

    Task startByIndex(final int index) throws IndexIncorrectException;

    Task startByName(final String name) throws TaskNotFoundException;

    Task finishById(final String id) throws TaskNotFoundException;

    Task finishByIndex(final int index) throws IndexIncorrectException;

    Task finishByName(final String name) throws TaskNotFoundException;

    Task updateStatusById(final String id, final Status status) throws TaskNotFoundException;

    Task updateStatusByIndex(final int index, final Status status) throws IndexIncorrectException;

    Task updateStatusByName(final String name, final Status status) throws TaskNotFoundException;

    Task bindTaskToProjectById(final String projectId, final String taskId) throws TaskNotFoundException;

    Task unbindTaskById(final String id) throws TaskNotFoundException;

    List<Task> findAllByProjectId(final String id) throws TaskNotFoundException;

    void removeAllTaskByProjectId(final String id);

}
