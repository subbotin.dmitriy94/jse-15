package com.tsconsulting.dsubbotin.tm.api.controller;

import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;

public interface IProjectTaskController {

    void bindTaskToProject() throws EmptyIdException, ProjectNotFoundException, TaskNotFoundException;

    void unbindTaskFromProject() throws EmptyIdException, ProjectNotFoundException, TaskNotFoundException;

    void findAllTasksByProjectId() throws EmptyIdException, ProjectNotFoundException, TaskNotFoundException;

    void removeProjectById() throws EmptyIdException, ProjectNotFoundException;

    void removeProjectByIndex() throws IndexIncorrectException;

    void removeProjectByName() throws ProjectNotFoundException, EmptyNameException;

}
