package com.tsconsulting.dsubbotin.tm.api.controller;

import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownStatusException;

public interface ITaskController {

    void createTask() throws EmptyNameException, EmptyDescriptionException;

    void showTasks() throws TaskNotFoundException;

    void clearTasks();

    void showById() throws EmptyIdException, TaskNotFoundException;

    void showByIndex() throws IndexIncorrectException, TaskNotFoundException;

    void showByName() throws EmptyNameException, TaskNotFoundException;

    void removeById() throws EmptyIdException, TaskNotFoundException;

    void removeByIndex() throws EmptyIndexException, IndexIncorrectException, TaskNotFoundException;

    void removeByName() throws EmptyNameException, TaskNotFoundException;

    void updateById() throws EmptyIdException, TaskNotFoundException, EmptyNameException;

    void updateByIndex() throws IndexIncorrectException, EmptyNameException, TaskNotFoundException;

    void startById() throws EmptyIdException, TaskNotFoundException;

    void startByIndex() throws IndexIncorrectException, TaskNotFoundException;

    void startByName() throws EmptyNameException, TaskNotFoundException;

    void finishById() throws EmptyIdException, TaskNotFoundException;

    void finishByIndex() throws IndexIncorrectException, TaskNotFoundException;

    void finishByName() throws EmptyNameException, TaskNotFoundException;

    void updateStatusById() throws EmptyIdException, TaskNotFoundException, UnknownStatusException;

    void updateStatusByIndex() throws IndexIncorrectException, UnknownStatusException;

    void updateStatusByName() throws EmptyNameException, TaskNotFoundException, UnknownStatusException;

}
