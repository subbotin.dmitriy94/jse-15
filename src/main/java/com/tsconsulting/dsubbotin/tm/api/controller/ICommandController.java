package com.tsconsulting.dsubbotin.tm.api.controller;

import com.tsconsulting.dsubbotin.tm.exception.system.UnknownArgumentException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownCommandException;

public interface ICommandController {

    void displayWelcome();

    void displayAbout();

    void displayVersion();

    void displayInfo();

    void displayHelp();

    void displayCommands();

    void displayArguments();

    void displayArgError() throws UnknownArgumentException;

    void displayCommandError() throws UnknownCommandException;

    void exit();

}
