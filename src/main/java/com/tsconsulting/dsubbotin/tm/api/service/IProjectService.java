package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void create(final String name);

    void create(final String name, final String description) throws EmptyNameException, EmptyDescriptionException;

    void add(final Project project) throws ProjectNotFoundException;

    void remove(final Project project) throws ProjectNotFoundException;

    List<Project> findAll();

    List<Project> findAll(final Comparator<Project> comparator);

    void clear();

    Project findById(final String id) throws EmptyIdException, ProjectNotFoundException;

    Project findByIndex(final int index) throws IndexIncorrectException;

    Project findByName(final String name) throws EmptyNameException, ProjectNotFoundException;

    Project updateById(final String id, final String name, final String description) throws EmptyIdException, EmptyNameException, ProjectNotFoundException;

    Project updateByIndex(final int index, final String name, final String description) throws EmptyIndexException, EmptyNameException, IndexIncorrectException;

    Project startById(final String id) throws EmptyIdException, ProjectNotFoundException;

    Project startByIndex(final int index) throws IndexIncorrectException;

    Project startByName(final String name) throws EmptyNameException, ProjectNotFoundException;

    Project finishById(final String id) throws EmptyIdException, ProjectNotFoundException;

    Project finishByIndex(final int index) throws IndexIncorrectException;

    Project finishByName(final String name) throws EmptyNameException, ProjectNotFoundException;

    Project updateStatusById(final String id, final Status status) throws EmptyIdException, ProjectNotFoundException;

    Project updateStatusByIndex(final int index, final Status status) throws IndexIncorrectException;

    Project updateStatusByName(final String name, final Status status) throws EmptyNameException, ProjectNotFoundException;

}
