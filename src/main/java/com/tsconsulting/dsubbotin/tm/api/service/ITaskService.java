package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void create(final String name) throws EmptyNameException;

    void create(final String name, final String description) throws EmptyDescriptionException, EmptyNameException;

    void add(final Task task) throws TaskNotFoundException;

    void remove(final Task task) throws TaskNotFoundException;

    List<Task> findAll();

    List<Task> findAll(final Comparator<Task> comparator);

    void clear();

    Task findById(final String id) throws EmptyIdException, TaskNotFoundException;

    Task findByIndex(final Integer index) throws IndexIncorrectException;

    Task findByName(final String name) throws EmptyNameException, TaskNotFoundException;

    Task removeById(final String id) throws TaskNotFoundException, EmptyIdException;

    Task removeByIndex(final int index) throws EmptyIndexException, IndexIncorrectException, TaskNotFoundException;

    Task removeByName(final String name) throws EmptyNameException, TaskNotFoundException;

    Task updateById(final String id, final String name, final String description) throws EmptyIdException, EmptyNameException, TaskNotFoundException;

    Task updateByIndex(final int index, final String name, final String description) throws IndexIncorrectException, EmptyNameException, TaskNotFoundException;

    Task startById(final String id) throws EmptyIdException, TaskNotFoundException;

    Task startByIndex(final int index) throws IndexIncorrectException, TaskNotFoundException;

    Task startByName(final String name) throws EmptyNameException, TaskNotFoundException;

    Task finishById(final String id) throws EmptyIdException, TaskNotFoundException;

    Task finishByIndex(final int index) throws IndexIncorrectException, TaskNotFoundException;

    Task finishByName(final String name) throws EmptyNameException, TaskNotFoundException;

    Task updateStatusById(final String id, final Status status) throws EmptyIdException, TaskNotFoundException;

    Task updateStatusByIndex(final int index, final Status status) throws IndexIncorrectException;

    Task updateStatusByName(final String name, final Status status) throws EmptyNameException, TaskNotFoundException;

}
