package com.tsconsulting.dsubbotin.tm.exception.entity;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Task not found!");
    }
}
