package com.tsconsulting.dsubbotin.tm.exception.system;

import com.tsconsulting.dsubbotin.tm.constant.TerminalConst;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Command not found! Use command '" + TerminalConst.HELP + "'.");
    }

}
